/**
 * Created by mateusz on 14.03.15.
 */
'use strict';
var http = require("http");
var url = require("url");
var httpPort = 8888;
function start(route,handle) {
    function onRequest(request, response) {
        var postData = "";
        var pathname = url.parse(request.url).pathname;
        var method = request.method.toLowerCase();
        console.log("Request for " + pathname + " received.Method: "+method);

        request.setEncoding("utf8");

        request.addListener("data", function(postDataChunk) {
            postData += postDataChunk;
            console.log("Received POST data chunk '"+
            postDataChunk + "'.");
        });
        request.addListener("end", function() {
            route(handle, pathname, response, postData);
        });

    }
    http.createServer(onRequest).listen(httpPort);
    console.log("Server has started: "+httpPort);
}
exports.start = start;